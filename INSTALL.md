#Install instructions for supported Operating Systems

# Recent Linux Distributions with packages for MetaBAT pre-requisites:

## Docker:
-----------------
```
git clone https://bitbucket.org/berkeleylab/metabat.git
cd metabat
docker build --tag metabat .
docker run metabat runMetaBat.sh ...

```

## Linux Ubuntu 14.04:
-----------------
```
#### install scons, boost and samtools library
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
sudo apt-get update
sudo apt-get install scons libboost-all-dev g++ libz-dev libncurses5-dev libbam-dev

#### download kseq.h
wget http://lh3lh3.users.sourceforge.net/download/kseq.tar.bz2

#### build metabat
cd metabat

#### download kseq.h (since ubuntu does not include this in libbam-dev)
cd src
wget http://lh3lh3.users.sourceforge.net/download/kseq.tar.bz2
tar -xvjf kseq.tar.bz2
cd ..

#### build metabat
mkdir $HOME/metabat
scons PREFIX=$HOME/metabat install

```

## Linux Fedora 20
--------------------

```
#### install g++, scons, boost and other dependencies
sudo yum install gcc-c++ scons boost.x86_64 boost-devel.x86_64 samtools-devel.x86_64 samtools-libs.x86_64 samtools.x86_64 zlib-devel.x86_64 libstdc++-static


# extract metabat
tar -xjf metabat-*.tar.gz
cd metabat-*

# compile test and install metabat
mkdir $HOME/metabat
scons PREFIX=$HOME/metabat install


```

# Older distributions must build and install:
```
gcc >= 4.9
boost >= 1.53
samtools >= 0.1.19 ( or samtools >= 1.0 with libhts >= 1.0 )
scons >= 2.1.0
```

## Linux CentOS 6.5
--------------------

```
# install g++ 4.7
sudo wget http://people.centos.org/tru/devtools-1.1/devtools-1.1.repo -O /etc/yum.repos.d/devtools-1.1.repo
sudo yum install devtoolset-1.1
sudo scl enable devtoolset-1.1 bash
export PATH=/opt/centos/devtoolset-1.1/root/usr/bin:$PATH

# install other prereques
sudo yum install scons zlib-devel.x86_64 ncurses-devel.x86_64 ncurses-term.x86_64 ncurses-base.x86_64 

# build boost 
wget http://downloads.sourceforge.net/project/boost/boost/1.55.0/boost_1_55_0.tar.gz
tar -xzf boost_1_55_0.tar.gz
cd boost_1_55_0
./bootstrap.sh
export BOOST_ROOT=$HOME/boost-gnu-4.7
./bjam --prefix=$BOOST_ROOT install && cd .. && rm -r boost_1_55_0

# extract metabat
tar -xjf metabat-*.tar.gz
cd metabat-*

# compile test and install metabat
mkdir $HOME/metabat
scons PREFIX=$HOME/metabat install

```


## MacOS X: (  using Homebrew http://brew.sh/ )
---------------------

```
#### install gcc 4.9 and scons
brew tap homebrew/versions
brew install gcc49 scons

#### install boost, compile with g++-4.9 (note you can not use homebrew's installation)
cd /tmp
curl -L -o boost_1_59_0.tar.gz http://downloads.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.tar.gz
tar -xzf boost_1_59_0.tar.gz
cd boost_1_59_0
echo "using gcc : 4.9 : /usr/local/bin/g++-4.9 ; " >> tools/build/user-config.jam
./bootstrap.sh
./bjam --toolset=gcc-4.9 --prefix=$HOME/boost-1.59.0-gnu-4.9 install && cd .. && rm -r boost_1_59_0

#### build metabat with scons
mkdir $HOME/metabat
CXX=g++-4.9 scons PREFIX=$HOME BOOST_ROOT=$HOME/boost-1.59.0-gnu-4.9 install
```
NOTE: all versions of HTSLIB are incompatable with SAMTOOLS <=0.1.19,
      so if you have HTSLIB installed please upgrade samtools to >=1.0,
      or to use samtools 0.1.19, specify a non-existant htslib install (i.e. HTSLIB_DIR=/dummy)

