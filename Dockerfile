FROM ubuntu:16.04 AS builder-env

LABEL Maintainer="Rob Egan<RSEgan@lbl.gov>"

WORKDIR /root

# This is necessary because the upgrade sometimes prompts for input
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update  && \
    apt-get install -y build-essential libboost-all-dev scons git curl libncurses5-dev zlib1g-dev

FROM builder-env as builder

# copy the git tree (minus Dockerfile) to metabat subdir
COPY . metabat

RUN cd metabat && \
    mkdir install && \
    scons install PREFIX=./install

FROM alpine:latest
WORKDIR /root
RUN apk add --no-cache bash
ENV PATH=$PATH:/root/bin
COPY --from=builder /root/metabat/install .

CMD ["./bin/metabat2"]

# build and deploy with this command
# docker build --tag robegan21/metabat:$(git describe --tags) --tag robegan21/metabat:latest . && docker push robegan21/metabat
