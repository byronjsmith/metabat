import os
import sys
import subprocess
import SCons
import platform

def ENV_update(tgt_ENV, src_ENV):
    for K in src_ENV.keys():
        if K in tgt_ENV.keys() and K in [ 'PATH', 'LD_LIBRARY_PATH', 'LIB', 'INCLUDE' ]:
            tgt_ENV[K]=SCons.Util.AppendPath(tgt_ENV[K], src_ENV[K])
        else:
            tgt_ENV[K]=src_ENV[K]

try:
    cxx = os.environ['CXX']
except KeyError:
    cxx = 'g++'

env = Environment();
ENV_update(env['ENV'], os.environ)
env.Replace(CXX=cxx)
opts = Variables('MetaBAT.conf')

debug = ARGUMENTS.get('DEBUG', None)

build_flags = ['-Wall', '-g', '-std=c++11', '-fopenmp']
link_flags = ['-lstdc++', '-lm', '-fopenmp']

if platform.platform(True, True).find('Darwin') == -1:
    link_flags.extend(['-static', '-static-libgcc', '-static-libstdc++'])

if debug is None:
    build_flags.extend(['-O3', '-DNDEBUG', '-Wno-unknown-pragmas', '-Wno-deprecated-declarations', '-Wno-overflow', '-Wno-unused-variable'])
else:
    build_flags.extend(['-Og'])

searchPaths = ['/usr', '/usr/local', '/opt']
 
def findCmdLineOrEnvOrPath(envname, searchPaths, testFile):
    var = ARGUMENTS.get(envname, None)
    if var is None:
        # attempt to get from environment
        try:
            var = os.environ[envname]
            print "Found from environment %s=%s" % (envname, var)
        except KeyError:
            # atempt to get from default installation paths
            for path in searchPaths:
                if os.path.isfile(path + testFile):
                    var = path
                    print "Found %s at %s" % (envname, var)
                    break
    else:
        if not os.path.isfile(var + testFile):
            print "WARNING: Could not find %s within %s=%s... Skipping." % (testFile, envname, var)
            var = None
        else:
            print "Found from command line %s=%s" % (envname, var)
    if var is None:
        print "WARNING: Could not find a valid path for %s" % (envname)
    return var


boost_root   = findCmdLineOrEnvOrPath('BOOST_ROOT', searchPaths, '/include/boost/unordered_map.hpp')
htslib_dir   = 'samtools'
samtools_dir = 'samtools'

if boost_root is None:
    print "ERROR: Could not find a valid boost installation.  Please install one and/or specify a BOOST_ROOT on the command line or environment"
    exit(1)

if not os.path.isfile(htslib_dir + "/lib/libhts.a") or not os.path.isfile(samtools_dir + "/lib/libbam.a"):
    print "Building with custom install of htslib / samtools"
    subprocess.call("./build_samtools.sh")

opts.Add(PathVariable('PREFIX', 'Directory to install under', '.', PathVariable.PathIsDir))
opts.Update(env)

Help(opts.GenerateHelpText(env))

libcpp = ''
if sys.platform == 'darwin':
    libcpp = '-lc++'

isOldSamtools = False
htslib_dir = 'samtools'
samtools_dir = 'samtools'
hts_lib = [ htslib_dir + "/lib", samtools_dir + "/lib" ]
hts_inc = [ htslib_dir + "/include", samtools_dir + "/include" ]
hts_flag = ''

idir_prefix = '$PREFIX'
idir_bin    = '$PREFIX/bin'

boost_lib   = boost_root + '/lib'
boost_inc   = boost_root + '/include'

test_boost_lib_paths = ('%s/x86_64-linux-gnu' % (boost_lib), boost_lib)
for test_dir in test_boost_lib_paths:
    if os.path.isdir( test_dir ):
        boost_lib = test_dir
        print "Found boost libs in %s" % (boost_lib)
        break


def findStaticOrShared( lib, testPaths, static_source_list, link_flag_list, staticSuffixes=['.a'] ):
    """Returns static_source_list, link_flag_list"""
    if not isinstance(testPaths, list):
        testPaths = [testPaths]
    for path in testPaths:
        if not os.path.isdir(path):
            continue
        for suffix in staticSuffixes:
            testfile = '%s/lib%s%s' % (path, lib, suffix)
            if os.path.isfile(testfile):
                static_source_list.append(testfile)
                print "Found static library %s as %s" % (lib, testfile)
                return
        for testfile in ('%s/lib%s.so' % (path, lib), '%s/lib%s.dylib' % (path, lib)):
            if os.path.isfile(testfile):
                print "Found shared library %s as %s" % (lib, testfile)
                link_flag_list.extend( ["-L%s" % (path), "-l%s" % (lib) ] )
                return
    print "Could not find library for %s!!! Looked in %s" % (lib, testPaths)
    return

for header in ('boost/unordered_map.hpp', 'boost/program_options.hpp'):
    testfile = boost_inc + '/' + header
    if not os.path.isfile(testfile):
        print "Error: Could not find boost header %s. Please install boost >= 1.55.0" % (header)

print ("setting boost include to %s" % (boost_inc))
print ("setting boost lib to %s" % (boost_lib))

test_bam = 'test/contigs-1000.fastq.bam'
test_fa  = 'test/contigs.fa'

test_depth = 'test/contigs.fa.depth.txt'
test_depth_pruned = test_depth + '.pruned'
test_bins = 'test/contigs.fa.bins.txt.fa'

Export('env idir_prefix idir_bin test_bam test_fa')

sources=['src/metabat1.cpp']
linkflags=link_flags
findStaticOrShared('boost_program_options', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_serialization', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_graph', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_system', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_filesystem', boost_lib, sources, linkflags, ['.a', '-mt.a'])

ccflags = build_flags
metabat1 = env.Program("metabat1", 
                sources,
                CCFLAGS=ccflags,
                CPPPATH=['./include', boost_inc],
                LINKFLAGS=linkflags,
                LIBS=['z']
                )
sources=['src/metabat2.cpp']
linkflags=link_flags
findStaticOrShared('boost_program_options', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_system', boost_lib, sources, linkflags, ['.a', '-mt.a'])
findStaticOrShared('boost_filesystem', boost_lib, sources, linkflags, ['.a', '-mt.a'])

ccflags = build_flags
metabat2 = env.Program("metabat2", 
                sources,
                CCFLAGS=ccflags,
                CPPPATH=['./include', boost_inc],
                LINKFLAGS=linkflags,
                LIBS=['z']
                )   

sources = ['src/jgi_summarize_bam_contig_depths.cpp']
linkflags=link_flags
findStaticOrShared('bam', hts_lib, sources, linkflags)
if not isOldSamtools:
    findStaticOrShared('hts', hts_lib, sources, linkflags)

ccflags = build_flags
ccflags.append(hts_flag)
jgi_summarize_bam_contig_depths = env.Program("jgi_summarize_bam_contig_depths",
                sources,
                CCFLAGS=ccflags,
                CPPPATH=[hts_inc, boost_inc],
                LIBS=['pthread', 'z'],
                LINKFLAGS=linkflags
                )

sources = ['src/contigOverlaps.cpp']
linkflags=link_flags
linkflags.extend(['-pthread', libcpp])
findStaticOrShared('boost_system', boost_lib, sources, linkflags)
findStaticOrShared('bam', hts_lib, sources, linkflags)
if not isOldSamtools:
    findStaticOrShared('hts', hts_lib, sources, linkflags)

ccflags = build_flags
ccflags.append(hts_flag)
contigOverlaps = env.Program("contigOverlaps",
                sources,
                CCFLAGS=ccflags,
                CPPPATH=[hts_inc, boost_inc],
                LINKPATH=[hts_lib, boost_lib],
                LINKFLAGS=linkflags,
                LIBS=['z']
                )

test_sum = env.Alias('test_sum', [jgi_summarize_bam_contig_depths],  ' '.join([jgi_summarize_bam_contig_depths[0].abspath, '--outputDepth', test_depth, test_bam]) )
AlwaysBuild(test_sum)
test_metabat = env.Alias('test_metabat', [metabat1, jgi_summarize_bam_contig_depths, test_sum], ' '.join([metabat1[0].abspath, '--inFile', test_fa, '--outFile', test_bins, '--abdFile', test_depth]) )
AlwaysBuild(test_metabat)

test = env.Alias('test', [test_sum, test_metabat])

Default(env.Command('metabat','metabat2','ln -s ${SOURCE.file} $TARGET'))

Default(env.Install(idir_bin, metabat1))
Default(env.Install(idir_bin, metabat2))
Default(env.Install(idir_bin, jgi_summarize_bam_contig_depths))
Default(env.Install(idir_bin, contigOverlaps))
Default(env.Install(idir_bin, 'runMetaBat.sh'))
Default(env.Install(idir_bin, 'aggregateBinDepths.pl'))
Default(env.Install(idir_bin, 'aggregateContigOverlapsByBin.pl'))

Default(env.Command(os.path.join(idir_bin, 'metabat'), os.path.join(idir_bin, 'metabat2'), 'ln -s ${SOURCE.file} $TARGET'))

env.Alias('install', [test, idir_prefix])
