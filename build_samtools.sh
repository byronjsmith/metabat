#!/bin/bash

oops() { echo "Failed to build samtools!" 1>&2 ; exit 1; }
trap oops 0

set -x
set -e

htslibver=1.2.1
samver=1.2
samtar=samtools-${samver}.tar.bz2
url=https://github.com/samtools/samtools/releases/download/${samver}/${samtar}
dir=samtools-$samver

[ -f $samtar ] || curl -kL -o $samtar $url
[ -d $dir ] || tar -xvjf $samtar

inst=`pwd`/samtools
for d in bin include include/bam lib lib/pkgconfig share share/man share/man/man5 share/man/man1
do
  mkdir -p $inst/$d
done

cd $dir

cd htslib-$htslibver
./configure --prefix=$inst
make && make install

cd ..

make
make prefix=$inst install
cp libbam.a $inst/lib
cp *.h $inst/include/bam


trap "" 0

